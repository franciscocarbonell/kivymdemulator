from kivy.lang import Builder
from kivy.clock import Clock

from kivymd.uix.screen import MDScreen


Builder.load_string(
'''

<MainScreenEmulator>
    MDBoxLayout:
        orientation: 'vertical'
        MDToolbar:
            id: toolbar
            title: 'KivyMDEmulator'
            left_action_items: [['menu', lambda x: app.root.ids.navigation_drawer.set_state()]]
        MDBoxLayout:
            padding: [5, 0, 5, 0]
            FloatLayout:
                MDBoxLayout:
                    id: box_emulator
                    pos_hint: {'center_x': 0.50, 'center_y': .514}
                    size_hint: .854, .798
                Image:
                    id: image_emulator
                    source: './assets_emulator/image_emulator.png'
                    pos_hint: {'center_x': 0.5, 'center_y': 0.5}
                    allow_stretch: True
                    keep_ratio: False
                    size_hint: None, None
                    height: root.height - dp(66)
                    width: root.width

'''
)

class WrapperScreenEmulator(MDScreen):
    def __init__(self, *args, **kwargs):
        _class = kwargs.pop('kivymd_class')
        self._class = _class
        return super(WrapperScreenEmulator, self).__init__(*args, **kwargs)

    def build(self):
        _class = self._class()
        return _class.build()


class MainScreenEmulator(MDScreen):
    def on_enter(self):
        Clock.schedule_once(self.post_enter)

    def post_enter(self, dt):
        pass
