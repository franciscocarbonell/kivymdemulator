import monkeypath

from kivymd.app import MDApp
from kivymd.uix.navigationdrawer import MDNavigationLayout
from kivymd.uix.chip import MDChip

from kivy.lang import Builder
from kivy.factory import Factory
from kivy.utils import platform
from kivy.core.window import Window
from kivy.animation import Animation

import string, os, subprocess
from context import app_context

Window.size = (325, 650)

Builder.load_string(
    '''
#: import MainScreenEmulator screens_emulator.main_screen
#: import ContentNavigationLayout screens_emulator.menu_emulator

<DriveChip>:
    size_hint_x: 1
    duration_long_touch: 0
    on_press: root.parent.removes_marks_all_chips(self)

<MainNavigationEmulator>:
    ScreenManager:
        id: screen_manager
        MainScreenEmulator:
            id: main_screen_emulator
        
    MDNavigationDrawer:
        id: navigation_drawer
        _drawer_emulator: True
        radius: 0
        ContentNavigationDrawerMenu:
            id: content_navigation

'''
)


class MainNavigationEmulator(MDNavigationLayout):
    pass


class DriveChip(MDChip):
    icon_check_color = (0, 0, 0, 1)
    text_color = (0, 0, 0, 0.5)
    _no_ripple_effect = True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.bind(active=self.set_chip_bg_color)
        self.bind(active=self.set_chip_text_color)

    def set_chip_bg_color(self, instance_chip, active_value):
        self.md_bg_color = (
            (0, 0, 0, 0.4)
            if active_value
            else (
                self.theme_cls.bg_darkest
                if self.theme_cls.theme_style == "Light"
                else (
                    self.theme_cls.bg_light
                    if not self.disabled
                    else self.theme_cls.disabled_hint_text_color
                )
            )
        )

    def set_chip_text_color(self, instance_chip, active_value):
        Animation(
            color=(0, 0, 0, 1) if active_value else (0, 0, 0, 0.5), d=0.2
        ).start(self.ids.label)


class MainWindowEmulator(MDApp):
    observer_handler = None

    def __init__(self, *args, **kwargs):
        _context = {'main_app': self}
        app_context.set(_context)
        return super(MainWindowEmulator, self).__init__(
            *args, **kwargs)

    def build(self):
        return Factory.MainNavigationEmulator()

    def on_start(self):
        self.check_plartform()

        title = self.__class__.__name__
        Window.set_title(title)
        return super(MainWindowEmulator, self).on_start()

    def on_top_window(self):
        process = subprocess.Popen(['wmctrl', '-l'], stdout=subprocess.PIPE)
        output = process.communicate()[0]
        lines = output.decode().split('\n')
        currnet_process = next((line for line in lines
            if self.__class__.__name__ in line), None)
        if currnet_process:
            title = currnet_process.split(' ')[-1]
            process = subprocess.Popen(
                ['wmctrl', '-r', title, '-b', 'toggle,above'],
                stdout=subprocess.PIPE)


    def check_plartform(self):
        if platform == 'win':
            from KivyOnTop import register_topmost
            register_topmost(Window, self.__class__.__name__)
            all_drives = string.ascii_uppercase
            drives = ['%s:' % drive for drive in all_drives \
                if os.path.exists('%s:' % drive)]
            self.set_drives(drives)

        if platform in ('linux', 'ios', 'android'):
            self.on_top_window()
            self.set_drives(['/'])

    def set_drives(self, drives):
        app = self.get_running_app()
        box_drives = app.root.ids.content_navigation.ids.box_drives
        for drive in drives:
            chip = DriveChip(text=drive)
            box_drives.add_widget(chip)

        chip = box_drives.children[-1]
        chip.active = True


if __name__ == '__main__':
    emulator = MainWindowEmulator()
    emulator.run()
