import click
from main_emulator import MainWindowEmulator


@click.group()
def cli():
    pass

@cli.command()
def run():
    MainWindowEmulator().run()

cli()
