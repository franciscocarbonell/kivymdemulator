from watchdog.events import FileSystemEventHandler, FileModifiedEvent


class FolderMonitor(FileSystemEventHandler):
    kivymd_app = None

    def __init__(self, kivymd_app):
        self.kivymd_app = kivymd_app
        return super(FolderMonitor, self).__init__()

    def on_any_event(self, event):
        if isinstance(event, FileModifiedEvent):
            self.kivymd_app.root.ids.content_navigation.rebuild(self.kivymd_app)
