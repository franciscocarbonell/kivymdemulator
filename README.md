## Recomendation
Create new virtualenv for installation (virtualenvwrapper or virtualenv)

## Python version
python 3.7 or later

## Installation
```
pip install requirements_(linux or window)
```
```
pip install kivymdemulator
```

## Dependencies for linux
```
sudo apt install wmctrl
```

## Run emulator
```
kivymdemulator run
```

![image](images/kivyemulator.gif)